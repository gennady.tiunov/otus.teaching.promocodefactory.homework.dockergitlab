FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
COPY ./src /app
WORKDIR /app/Otus.Teaching.PromoCodeFactory.WebHost
RUN dotnet restore
RUN dotnet publish -c Release -o out --no-restore

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS runtime
WORKDIR /app
COPY --from=build /app/Otus.Teaching.PromoCodeFactory.WebHost/out ./
EXPOSE 3333
ENTRYPOINT ["dotnet", "Otus.Teaching.PromoCodeFactory.WebHost.dll"]
