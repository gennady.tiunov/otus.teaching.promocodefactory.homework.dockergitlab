using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers().AddMvcOptions(x => x.SuppressAsyncSuffixInActionNames = false);
builder.Services.AddSingleton(typeof(IRepository<>), typeof(EfRepository<>));

builder.Services.AddSingleton<IDbInitializer, EfDbInitializer>();
builder.Services.AddDbContext<DataContext>(x =>
{

    x.UseNpgsql(builder.Configuration["ConnectionStrings:DockerLabDB"]);
    x.UseLazyLoadingProxies();
}, ServiceLifetime.Singleton);

builder.Services.AddOpenApiDocument(options =>
{
    options.Title = "PromoCode Factory API Doc";
    options.Version = "1.0";
});

//builder.WebHost.UseKestrel().UseUrls($"http://0.0.0.0:3333").CaptureStartupErrors(true);
//builder.WebHost.ConfigureKestrel(options => options.ListenLocalhost(3333));

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseHsts();
}

app.UseOpenApi();
app.UseSwaggerUi3(x =>
{
    x.DocExpansion = "list";
});

app.UseHttpsRedirection();

app.UseRouting();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});

app.Services.GetRequiredService<IDbInitializer>().InitializeDb();

app.Run();